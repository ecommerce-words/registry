init: tidy docker-down docker-up build run

run:
	./registry

build:
	go build -v ./cmd/registry

tidy:
	go mod tidy

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down

swagger:
	swag init -g internal/server/server.go

kafka-consume-registry:
	docker-compose exec kafka  kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic registry --from-beginning

.PHONY: init run build tidy docker-down docker-up swagger kafka-consume-registry
.DEFAULT_GOAL := init