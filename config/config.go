package config

import (
	"log"

	"github.com/spf13/viper"
)

// Config App config struct
type Config struct {
	Server       ServerConfig
	MongoStorage MongoStorageConfig
	Kafka        KafkaConfig
}

type ServerConfig struct {
	AppVersion string
	Port       string
	Host       string
	BasePath   string
}

type MongoStorageConfig struct {
	ConnStr string
}

type KafkaConfig struct {
	Brokers []string
	Topic   string
}

func BuildConfig(filename string) (*Config, error) {
	v, err := newViper(filename)
	if err != nil {
		return nil, err
	}

	return parseConfig(v)
}

// newViper Build new Viper instance
func newViper(filename string) (*viper.Viper, error) {
	v := viper.New()

	v.SetConfigFile(filename)
	v.AutomaticEnv()
	v.WatchConfig()

	if err := v.ReadInConfig(); err != nil {
		return nil, err
	}

	return v, nil
}

// parseConfig Parse config file
func parseConfig(v *viper.Viper) (*Config, error) {
	var c Config

	err := v.Unmarshal(&c)
	if err != nil {
		log.Printf("unable to decode into struct, %v", err)

		return nil, err
	}

	return &c, nil
}
