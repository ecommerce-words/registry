package kafka

import (
	"context"
	"errors"

	"gitlab.com/ecommerce-words/registry/pkg/logger"

	"github.com/segmentio/kafka-go"
)

type Client struct {
	Logger *logger.Logger
	Writer *kafka.Writer
}

// NewClient Create and init client Kafka
func NewClient(brokers []string, topic string) (*Client, error) {
	if len(brokers) == 0 || brokers[0] == "" || topic == "" {
		return nil, errors.New("kafka connection parameters not specified")
	}

	return &Client{
		Writer: &kafka.Writer{
			Addr:     kafka.TCP(brokers...),
			Topic:    topic,
			Balancer: &kafka.LeastBytes{},
		},
	}, nil
}

// SendMessages Send messages to kafka
func (c *Client) SendMessages(ctx context.Context, messages []kafka.Message) error {
	return c.Writer.WriteMessages(ctx, messages...)
}
