package mongodb

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type DB struct {
	*mongo.Database
}

func Open(ctx context.Context, connStr, dbStr string) (*DB, error) {
	cOpts := options.Client()
	cOpts.ApplyURI(connStr)

	mClient, err := mongo.Connect(ctx, cOpts)
	if err != nil {
		return nil, fmt.Errorf("unable to connect: %v", err)
	}

	if err := mClient.Ping(ctx, nil); err != nil {
		return nil, err
	}

	return &DB{mClient.Database(dbStr)}, nil
}

func (db *DB) Close(ctx context.Context) {
	if err := db.Client().Disconnect(ctx); err != nil {
		log.Fatal(err)
	}
}
