package logger

import (
	"log"

	"go.uber.org/zap"
)

type Logger struct {
	*zap.SugaredLogger
}

func Init() *Logger {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("can't initialize zap logger: %v", err)
	}

	return &Logger{logger.Sugar()}
}
