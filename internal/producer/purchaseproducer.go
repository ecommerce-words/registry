package producer

import (
	"context"
	"encoding/json"
	"time"

	"gitlab.com/ecommerce-words/registry/pkg/logger"

	"gitlab.com/ecommerce-words/registry/internal/model"

	kafkago "github.com/segmentio/kafka-go"
	"gitlab.com/ecommerce-words/registry/pkg/kafka"
)

const EventName = "purchaseRequested"

type Message struct {
	EventName string          `json:"event_name"`
	EventTime time.Time       `json:"event_time"`
	Payload   *model.Purchase `json:"payload"`
}

type Producer struct {
	logger *logger.Logger
	kfk    *kafka.Client
}

func NewProducer(logger *logger.Logger, kfk *kafka.Client) *Producer {
	return &Producer{
		logger: logger,
		kfk:    kfk,
	}
}

func (p *Producer) Produce(payload *model.Purchase) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	value, err := json.Marshal(&Message{
		EventName: EventName,
		EventTime: time.Now(),
		Payload:   payload,
	})
	if err != nil {
		p.logger.Errorf("error send messages to kafka: %v, value: %v", err, value)

		return
	}

	messages := []kafkago.Message{
		{
			Value: value,
			Time:  time.Now(),
		},
	}

	err = p.kfk.SendMessages(ctx, messages)
	if err != nil {
		p.logger.Errorf("error send messages to kafka: %v, value: %v", err, value)
	}
}
