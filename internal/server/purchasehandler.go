package server

import (
	"context"
	"net/http"
	"time"

	"github.com/go-chi/render"
	"gitlab.com/ecommerce-words/registry/internal/model"
)

type Request struct {
	UserID  string `json:"user_id" validate:"required,uuid4" example:"51bf5c8a-0ec4-4221-bbf8-118e57db6d6c"`
	Product string `json:"product" validate:"required" example:"A"`
}

// @Summary      Purchase
// @Description  Product purchase request
// @Tags         Purchase
// @Accept       json
// @Produce      json
// @Param        request body Request true "Request"
// @Success      200    "OK"
// @Failure      422    "Validation error"
// @Failure      500    "Server Error"
// @Router       /purchase [POST]
func (s *APIServer) purchaseHandler() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		req := &Request{}

		// decode body to request
		err := render.DecodeJSON(r.Body, req)
		if err != nil {
			render.Status(r, http.StatusBadRequest)
			render.JSON(w, r, render.M{"error": err.Error()})

			return
		}

		// validate request
		valErrs, err := Validate(req)
		if err != nil {
			s.logger.Errorf("invalid validation error: %v", err)

			render.Status(r, http.StatusInternalServerError)
			render.JSON(w, r, render.M{"error": "server error"})

			return
		}

		if valErrs != nil {
			render.Status(r, http.StatusUnprocessableEntity)
			render.JSON(w, r, valErrs)

			return
		}

		// create purchase record
		ctx, cancel := context.WithTimeout(r.Context(), 5*time.Second)
		defer cancel()

		purchase := model.NewPurchase(req.UserID, req.Product)
		if err = s.purchaseRepo.Create(ctx, purchase); err != nil {
			s.logger.Errorf("failed create purchase: %v", err)

			render.Status(r, http.StatusInternalServerError)
			render.JSON(w, r, render.M{"error": "server error"})

			return
		}

		// send to kafka
		go s.registryProducer.Produce(purchase)

		render.Status(r, http.StatusOK)
		render.JSON(w, r, render.M{"message": "success"})
	}
}
