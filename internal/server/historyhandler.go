package server

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/render"
)

// @Summary      History
// @Description  History of product purchase requests
// @Tags         History
// @Accept       json
// @Produce      json
// @Param        user_id query string true "User ID"
// @Param        page    query string true "Page" default(1)
// @Param        limit   query string true "Limit" default(10)
// @Success      200    "OK"
// @Failure      422    "Incorrect redirect uri"
// @Failure      500    "Server Error"
// @Router       /history [GET]
func (s *APIServer) historyHandler() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(r.Context(), 5*time.Second)
		defer cancel()

		userId := r.FormValue("user_id")
		if userId == "" {
			render.Status(r, http.StatusUnprocessableEntity)
			render.JSON(w, r, render.M{"error": "query parameter 'user_id' is required"})

			return
		}

		page, limit := getPageAndLimit(r)
		res, _ := s.purchaseRepo.FindByUserID(ctx, userId, page, limit)

		render.Status(r, http.StatusOK)
		render.JSON(w, r, res)
	}
}

func getPageAndLimit(r *http.Request) (convertedPageInt int, convertedLimitInt int) {
	queryPageValue := r.FormValue("page")
	if queryPageValue != "" {
		convertedPageInt, _ = strconv.Atoi(queryPageValue)
	}

	queryLimitValue := r.FormValue("limit")
	if queryLimitValue != "" {
		convertedLimitInt, _ = strconv.Atoi(queryLimitValue)
	}

	return convertedPageInt, convertedLimitInt
}
