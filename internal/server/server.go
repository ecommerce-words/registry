package server

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/ecommerce-words/registry/internal/producer"

	"gitlab.com/ecommerce-words/registry/docs"

	"gitlab.com/ecommerce-words/registry/config"
	"gitlab.com/ecommerce-words/registry/internal/storage"

	"gitlab.com/ecommerce-words/registry/pkg/logger"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/swaggo/http-swagger"
)

// @title        Registry API
// @description  This is a service for registering user requests for the purchase of product.

type APIServer struct {
	server           *http.Server
	config           *config.Config
	logger           *logger.Logger
	registryProducer *producer.Producer
	purchaseRepo     storage.PurchaseRepository
}

func NewServer(
	config *config.Config,
	logger *logger.Logger,
	registryProducer *producer.Producer,
	purchaseRepo storage.PurchaseRepository,
) *APIServer {
	return &APIServer{
		config:           config,
		logger:           logger,
		registryProducer: registryProducer,
		purchaseRepo:     purchaseRepo,
	}
}

func (s *APIServer) Start() {
	s.server = &http.Server{
		Addr:    s.config.Server.Host,
		Handler: s.configureRouter(),
	}

	err := s.server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}
}

func (s *APIServer) Stop(ctx context.Context) {
	err := s.server.Shutdown(ctx)
	if err != nil {
		log.Fatal(err)
	}
}

func (s *APIServer) configureRouter() http.Handler {
	r := chi.NewRouter()

	// middlewares
	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Get("/history", s.historyHandler())
	r.Post("/purchase", s.purchaseHandler())

	// swagger routes
	docs.SwaggerInfo.Host = s.config.Server.Host
	docs.SwaggerInfo.BasePath = s.config.Server.BasePath
	docs.SwaggerInfo.Version = s.config.Server.AppVersion
	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("/swagger/doc.json")),
	)

	return r
}
