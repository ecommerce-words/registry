package storage

import (
	"context"

	mongopagination "github.com/gobeam/mongo-go-pagination"

	"gitlab.com/ecommerce-words/registry/internal/model"
)

type Purchases struct {
	Data       []*model.Purchase              `json:"data"`
	Pagination mongopagination.PaginationData `json:"pagination"`
}

type PurchaseRepository interface {
	Create(context.Context, *model.Purchase) error
	FindByUserID(ctx context.Context, userID string, page, limit int) (*Purchases, error)
}
