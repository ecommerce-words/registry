package mdb

import (
	"context"
	"fmt"

	"gitlab.com/ecommerce-words/registry/internal/storage"

	mongopagination "github.com/gobeam/mongo-go-pagination"

	"gitlab.com/ecommerce-words/registry/internal/model"

	"gitlab.com/ecommerce-words/registry/pkg/mongodb"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type PurchaseRepository struct {
	purchases *mongo.Collection
}

func NewPurchaseRepository(db *mongodb.DB) *PurchaseRepository {
	return &PurchaseRepository{
		purchases: db.Collection("purchase_requests"),
	}
}

func (r *PurchaseRepository) Create(ctx context.Context, user *model.Purchase) error {
	_, err := r.purchases.InsertOne(ctx, user)

	return err
}

func (r *PurchaseRepository) FindByUserID(ctx context.Context, userID string, page, limit int) (*storage.Purchases, error) {
	filter := bson.D{
		bson.E{Key: "user_id", Value: bson.D{bson.E{Key: "$eq", Value: userID}}},
	}

	var data []*model.Purchase
	paginatedData, err := mongopagination.New(r.purchases).
		Context(ctx).
		Limit(int64(limit)).
		Page(int64(page)).
		Sort("created_at", -1).
		Filter(filter).
		Decode(&data).
		Find()
	if err != nil {
		return nil, fmt.Errorf("error find purchases: %v", err)
	}

	result := &storage.Purchases{
		Pagination: paginatedData.Pagination,
		Data:       data,
	}

	return result, nil
}
