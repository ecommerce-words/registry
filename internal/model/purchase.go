package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Purchase struct {
	ID        primitive.ObjectID `json:"id"         bson:"_id"`
	UserID    string             `json:"user_id"    bson:"user_id"`
	Product   string             `json:"product"    bson:"product"`
	CreatedAt time.Time          `json:"created_at" bson:"created_at"`
}

func NewPurchase(userId, product string) *Purchase {
	return &Purchase{
		ID:        primitive.NewObjectID(),
		UserID:    userId,
		Product:   product,
		CreatedAt: time.Now(),
	}
}
