db = db.getSiblingDB('registry');
db.createUser(
    {
        user: 'registry_user',
        pwd: 'secret',
        roles: [{ role: 'readWrite', db: 'registry' }],
    },
);
db.createCollection('purchase_requests');
