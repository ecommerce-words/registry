package main

import (
	"context"
	"os"
	"os/signal"
	"time"

	"gitlab.com/ecommerce-words/registry/internal/producer"

	"gitlab.com/ecommerce-words/registry/pkg/kafka"

	"gitlab.com/ecommerce-words/registry/internal/storage/mdb"

	"gitlab.com/ecommerce-words/registry/config"

	"gitlab.com/ecommerce-words/registry/pkg/logger"

	"gitlab.com/ecommerce-words/registry/pkg/mongodb"

	"gitlab.com/ecommerce-words/registry/internal/server"
)

func main() {
	serverCtx, serverStopCtx := context.WithCancel(context.Background())

	log := logger.Init()
	defer func() {
		_ = log.Sync()
	}()

	// configuration
	conf, err := config.BuildConfig("./config/registry-common.yml")
	if err != nil {
		log.Fatal(err)
	}

	log.Infof("start application version: %s", conf.Server.AppVersion)

	// db connection
	db, err := mongodb.Open(serverCtx, conf.MongoStorage.ConnStr, "registry")
	if err != nil {
		log.Fatalf("database not started with err: %v", err)
	}
	defer db.Close(context.Background())

	log.Infof("database connected via uri: %s", conf.MongoStorage.ConnStr)

	// kafka client
	kClient, err := kafka.NewClient(conf.Kafka.Brokers, conf.Kafka.Topic)
	if err != nil {
		log.Fatalf("error init kafka client: %v", err)
	}

	log.Infof("kafka client init via brokers: %v", conf.Kafka.Brokers)

	// create server
	s := server.NewServer(
		conf,
		log,
		producer.NewProducer(log, kClient),
		mdb.NewPurchaseRepository(db),
	)

	// graceful shutdown
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)
	go func() {
		<-sig

		// Shutdown signal with grace period of 30 seconds
		shutdownCtx, shutdownStopCtx := context.WithTimeout(serverCtx, 30*time.Second)
		defer shutdownStopCtx()

		go func() {
			<-shutdownCtx.Done()

			if shutdownCtx.Err() == context.DeadlineExceeded {
				log.Fatal("graceful shutdown timed out.. forcing exit.")
			}
		}()

		s.Stop(shutdownCtx)

		serverStopCtx()

		log.Info("gracefully shutting down")
	}()

	log.Info("start server")
	s.Start()

	// Wait for server context to be stopped
	<-serverCtx.Done()
}
